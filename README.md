# WebMario

# Software Studio 2020 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|N|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description

# Basic Components Description : 

![](https://imgur.com/p6KjpwN.jpg)

1. World map : [The world is made from Tile, I use the same as TA, I could not find more inspiration. The ground has good gravity and player can fall because of gravity.]
2. Player : [The player can jump, walk, it has perfect collider, and can only stand on the top of block and cylinder]
3. Enemies : [Enemies are goomba and flower]
4. Question Blocks : [Questions blocks have animation and can update the score onbegin contact with player.]
5. Animations : [Players have jump animations, walk animations, enemies have die animations]
6. Sound effects : [There is a bgm for all the game, and player has different sound effect for different movement, enemies has different sound effect for different actions, sound effect cannot stop the bgm of the game.]
7. UI : ![](https://imgur.com/qzsQNw3.jpg)
8. User can wether login or signup
 ![](https://imgur.com/PD1gNVZ.jpg)
12.  if user already have account can dirrectly login adn after user login switch scene ,alert message that user login. You can close the login box using the multiple in the top right corner.
 ![](https://imgur.com/ioCiief.jpg)
15. if user doesn't have an account, can signep up for free, alert message signed up succesfully and switch scene. You can close signup box using the multiple in the top right corner.
 ![](https://imgur.com/oTx2K1N.jpg)
17. user can chose a stage, that will load the game start scene
 ![](https://imgur.com/tNq5Jvy.jpg)
19. the game start scene stop all music and schedule a time to load after the game. An as in the top you can start playing.
 ![](https://imgur.com/yE1tqE6.jpg)
22. that is the game over scene if the player died
 ![](https://imgur.com/mNBShsk.jpg).
24. Player info are written and read to and from the database



# Bonus Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]


:::info
:pushpin: Want to learn more? ➜ [https://webmario-740ed.web.app/](https://webmario-740ed.web.app/) 
:::



